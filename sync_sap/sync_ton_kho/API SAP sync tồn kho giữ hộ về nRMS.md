# API SAP sync tồn kho giữ hộ về nRMS

## 1. Tổng quan

- nRMS viết API
- SAP gọi để sync tồn kho của đại lý sang khi có thay đổi. 
	- Tồn kho khi sync qua là lượng tồn kho cuối của đại lý.
	- Mỗi item trong list data là 1 lô.
- nRMS tiến hành xử lý dữ liệu
	- group by các record theo key (`customer`, `material`, `month_year`, `ma_ttpp`, `sloc`)
	- Sử dụng trường `month_year` làm mã lô (`batch` = `month_year`)
	- Ngày hết hạn giữ hộ = ngày cuối tháng của tháng năm trong `month_year` (hết hạn chỉ theo tháng và năm)
	- Số lượng tồn kho = sum(`quantity_in_base_unit`)
	- Đổi các trường customer và material từ mã SAP sang mã nRMS.


## 2. API
```
URL PRODUCTION: ___
```

```
URL TEST: http://61.28.236.241:10003/sapservice/inventory/syn_inventory_keep_to_nrms
```

```
TOKEN: e0a08376fe5c0b857ca34c11373d6c58
```

```
METHOD: POST
```

```JSON
BODY
{
	data: [
	    {
	        "ma_ttpp": "A100", // Mã TTPP
	        "sloc": "A101", // Mã kho
	        "customer": "3100000040", // Mã đại lý SAP
	        "material": "120000003", // Mã sản phẩm SAP
	        "batch": "0000002323", // Mã lô
	        "status_pro_in_use": "Unrestricted Use", // Trạng thái hàng hóa
	        "quantity_in_base_unit": 0, // Số lượng tồn kho quy cách nhỏ
	        "base_unit": "TUI", // Đơn vị tính quy cách nhỏ
	        "date_of_manufacture": "", // Ngày sản xuất
	        "expiration_date": "", // Ngày hết hạn giữ hộ
	        "humidity": "", // Tỷ lệ nảy mầm
	        "month_year": "", // Thuộc tính tháng năm
	        "intagration_status": "S", // Tình trạng đồng bộ dữ liệu
	    },
	],
	token: "string",
}
```