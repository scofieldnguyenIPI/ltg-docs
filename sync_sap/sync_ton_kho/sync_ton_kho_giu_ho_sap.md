# TÀI LIỆU SYNC TỒN KHO GIỮ HỘ TỪ SAP -> NRMS

## 1. Tổng quan

- nRMS viết API
- SAP gọi để sync tồn kho của đại lý sang khi có thay đổi. Tồn kho khi sync qua là lượng tồn kho cuối của đại lý.

## 2. Luồng hoạt động

```mermaid
flowchart LR;
    subgraph nRMS
        direction LR;
        A[(inventory_keep_data_sync)]
        B[(inventory_keep_temp)]
        C[(warehouse_keep_release)]
        A --> B --> C 
    end

    D[SAP] --API--> A;
```

Trong đó:

- inventory_keep_data_sync: bảng chứa log data sync từ SAP về. Data trong bảng chứa những lô bên SAP thay đổi.
- inventory_keep_temp: bảng tạm. Nối full lô của đại lý. Giả lập 1 lần sync full của SAP. (Do SAP chỉ sync những lô thay đổi mà không sync full tồn kho của đại lý)
- warehouse_keep_release: bảng chính chứa tồn kho của đại lý.

## 3. Thiết kế DB

db name:  `loctroi_sap`

table name: `inventory_keep`

| column | type | comment |
| --- | --- | --- |
| `id` | number | auto tăng |
| `ma_ttpp` | text | Mã TTPP |
| `sloc` | text | Mã kho |
| `material` | text | Mã sản phẩm SAP |
| `customer` | text | Mã đại lý SAP |
| `batch` | text | Mã lô |
| `status_pro_in_use` | text | Trạng thái hàng hóa được sử dụng |
| `quantity_in_base_unit` | text | __ |
| `base_unit` | text | đơn vị tính theo quy cách nhỏ |
| `date_of_manufacture` | datetime | thông tin ngày sản xuất của lô hàng trên SAP |
| `month_year` | text | Thông tin tháng năm của lô hàng |
| `expiration_date` | datetime | Ngày hết hạn của lô hàng |
| `humidity` | text | __ |
| `intagration_status` | text | Tình trạng đồng bộ dữ liệu |
| `error_note` | text | Mô tả tình trạng đồng bộ không thành công |
| `data_status` | text | __ |
| `syn_code` | text | -- |
| `create_date` | datetime | Ngày tạo |
| `update_date` | datetime | Ngày update |
| `status_id` | number | 1: Mới kéo về; 2: sync thành công; 3: thất bại |


## 4. API SAP sync tồn kho giữ hộ về nRMS

```
URL PRODUCTION: ___
```

```
URL TEST: ___
```

```
METHOD: POST
```

```JSON
BODY

[
    {
        "ma_ttpp": "A100", // Mã TTPP
        "sloc": "A101", // Mã kho
        "customer": "3100000040", // Mã đại lý SAP
        "material": "120000003", // Mã sản phẩm SAP
        "batch": "0000002323", // Mã lô
        "status_pro_in_use": "Unrestricted Use", // Trạng thái hàng hóa
        "quantity_in_base_unit": 0, // Số lượng tồn kho quy cách nhỏ
        "base_unit": "TUI", // Đơn vị tính quy cách nhỏ
        "date_of_manufacture": "", // Ngày sản xuất
        "expiration_date": "", // Ngày hết hạn giữ hộ
        "humidity": "", // Tỷ lệ nảy mầm
        "month_year": "", // Thuộc tính tháng năm
        "intagration_status": "S", // Tình trạng đồng bộ dữ liệu
    }
]
```

## 5. Rule cập nhật từ bảng `inventory_keep_data_sync` sang `inventory_keep_temp`

```
- Lấy 1 dòng dữ liệu từ bảng inventory_keep_data_sync => tìm ra các dòng chung mã đại lý.
- Tìm lần sync full gần nhất theo mã đại lý trong bảng inventory_keep_temp
- Dùng các dòng dữ liệu ở bảng inventory_keep_data_sync đè vào dòng dữ liệu đã lấy ra ở bảng inventory_keep_temp theo key (mã đại lý, mã sản phẩm, mã lô, mã plant, mã sloc) => nếu trùng thì đè, không trùng thì tạo dòng mới.
- Tiếp tục quay lại bước 1 cho đến khi hết 1 lần sync của SAP.
```

## 6. Rule cập nhật từ bảng `inventory_keep_temp` vào `warehouse_keep_release`

```
- Lấy các record ở lần sync full bảng inventory_keep_temp
- Xử lý dữ liệu đã query
    - group by các record theo key (customer, material, month_year, ma_ttpp, sloc)
    - Sử dụng trường month_year làm mã lô (batch = month_year)
    - Ngày hết hạn giữ hộ = ngày cuối tháng của tháng năm trong month_year (hết hạn chỉ theo tháng và năm)
    - Số lượng tồn kho = sum(quantity_in_base_unit)
    - Đổi các trường customer và material từ mã SAP sang mã nRMS.
- Clear tồn giữ hộ hiện tại của đại lý, sử dụng tồn đã group by và update vào
```