
# Api synd đơn trả giữ hộ ở trạng thái chờ giao hàng sang SAP và TMS

## 1. Tổng quan

- nRMS gọi api của SAP và TMS để đẩy đơn trả giữ hộ ở trạng thái chờ giao hàng sang bên SAP và TMS
- nRMS tiến hành xử lý 
	- Khi có đơn hàng TGH ở trạng thái chờ giao hàng.
	- nRMS tiến hành đẩy đơn qua SAP tách đơn theo ngành hàng  và TTPP 
	- Sau khi đẩy qua SAP thành công nRMS lưu lại SO_line sau đó đẩy đơn hàng qua TMS

## 2. Thiết kế DB

db name:  `loctroi_sap`

table name: `sale_order`

| column | type | comment |
| --- | --- | --- |
| `id` | number | auto tăng |
| `sold_to_party` | text | Mã đại lý sap |
| `po_number` | text | Mã đơn hàng nRMS |
| `header_id` | text | __ |
| `po_date` | text | ngày đẩy đơn |
| `so_type` | text | loại đơn (theo SAP) |
| `data_status` | text | Trạng thái đẩy đơn |
| `sale_organization` | text | Mã ttpp |
| `distribution_channel` | text | __ |
| `currency` | text | đơn vị tiền tệ |
| `fullname` | text | tên đại lý |
| `address` | text | địa điểm giao hàng |
| `order_phone` | text | số điện thoại giao hàng |
| `payment_type` | text | loại phương thức thanh toán |
| `status` | int | trạng thái đẩy đơn |
| `error_note` | text | lỗi khi đẩy đơn |
| `plan_code` | text | mã chi nhánh |
| `so_line` | text | So_line của SAP khi sync đơn thành công |
| `tms_error_note` | text | Lỗi khi đẩy sang TMS |
| `create_date` | datetime | ngày tạo |
| `update_time` | timestamp | ngày sửa |
| `payment_term_header` | text | __ |
| `notes` | text | ghi chú đơn hàng |
| `coordinate_time_confirm` | text | Thời gian Chi nhánh phản hồi đơn đã điều phối |
| `do_code` | text | mã TTPP |
| `vendor` | text | mã vendor TTPP |
| `purchasing_org` | text | mã purchasing_org TTPP |
| `company_code` | text | mã company_code TTPP |
| `dc_name` | text | tên TTPP |

table name: `sale_order_do_order_keep_return_log`

| column | type | comment |
| --- | --- | --- |
| `id` | number | auto tăng |
| `sold_to_party` | text | Mã đại lý sap |
| `po_number` | text | Mã đơn hàng nRMS |
| `header_id` | text | __ |
| `po_date` | text | ngày đẩy đơn |
| `so_type` | text | loại đơn (theo SAP) |
| `data_status` | text | Trạng thái đẩy đơn |
| `sale_organization` | text | Mã ttpp |
| `distribution_channel` | text | __ |
| `currency` | text | đơn vị tiền tệ |
| `fullname` | text | tên đại lý |
| `address` | text | địa điểm giao hàng |
| `order_phone` | text | số điện thoại giao hàng |
| `payment_type` | text | loại phương thức thanh toán |
| `status` | int | trạng thái đẩy đơn |
| `error_note` | text | lỗi khi đẩy đơn |
| `plan_code` | text | mã chi nhánh |
| `so_line` | text | So_line của SAP khi sync đơn thành công |
| `tms_error_note` | text | Lỗi khi đẩy sang TMS |
| `create_date` | datetime | ngày tạo |
| `update_time` | timestamp | ngày sửa |
| `payment_term_header` | text | __ |
| `notes` | text | ghi chú đơn hàng |
| `coordinate_time_confirm` | text | Thời gian Chi nhánh phản hồi đơn đã điều phối |
| `do_code` | text | mã TTPP |
| `vendor` | text | mã vendor TTPP |
| `purchasing_org` | text | mã purchasing_org TTPP |
| `company_code` | text | mã company_code TTPP |
| `dc_name` | text | tên TTPP |

table name: `sale_order_product_line`

| column | type | comment |
| --- | --- | --- |
| `id` | number | auto tăng |
| `sale_order_id` | int | id của table sale_order |
| `row_id` | text | id dòng đơn hàng của SAP |
| `storage_location` | text | mã kho |
| `material` | text | ngày sản phẩm SAP |
| `payment_item` | text | phương thức thanh toán (theo SAP) |
| `item_category` | text | ngành hàng (theo SAP) |
| `base_unit` | text | accesstory (theo SAP) |
| `order_quantity` | text | số lượng sản phẩm |
| `sale_unit` | text | đơn vị của order_quantity |
| `order_quantity_su` | text | số lượng khi đổi qua quy cách lớn |
| `pricing_condition_unit` | text | = sale_unit |
| `pricing_amount_1` | text | giá tiền 1 |
| `pricing_amount_2` | text | giá tiền 2 |
| `net_value` | text | tổng tiền |
| `discount_condition_unit` | text | đơn vị quà tặng |
| `discount_amount` | text | tiền khuyến mãi |
| `discount_value` | text | tổng khuyến mãi |
| `tax_rate` | text | % vat |
| `create_date` | datetime | ngày tạo |
| `update_time` | timestamp | ngày sửa |
| `tax_value` | text | tổng tiền thuế |
| `total_value` | text | tổng tiền cuối cùng |
| `zsolo` | text | mã lô |
| `so_line` | text | mã SO |
| `so_date` | text | __ |
| `gross_weight` | text | tổng trọng lượng |
| `zsolo_detail` | text | Chi tiết lô theo kho |
| `so_kygui` | text | __ |
| `plant` | text | mã TTPP |
| `batch` | text | mã lô |
| `req_deli_date` | text | ngày sync |

Trong đó:

- sale_order: đơn hàng TGH đẩy qua SAP và TMS
- sale_order_do_order_keep_return_log: Vì đơn hàng TGH cần tách đơn theo TTPP nên phải tách lần đẩy qua SAP, bảng này lưu log sync
- sale_order_product_line: bảng lưu thông tin sản phẩm của đơn hàng đẩy sang SAP và TMS

## 3. API của SAP


```
URL PRODUCTION: https://prdltgwdp.loctroi.vn:44303/sap/ZNRMS/ZWS_NRMS_PURCHASING_ORDER?sap-client=100
```

```
URL TEST: https://devltgwdp.loctroi.vn:44301/sap/ZNRMS/ZWS_NRMS_PURCHASING_ORDER?sap-client=300
```

```
METHOD: POST
```

```JSON
BODY

{
	"PO_NUMBER": "AX12-6668", //  ma don hang nRMS,
	"VENDOR": "3300001012", //= sold to party = ma SP cua dai ly,
	"PO_DATE": "08/12/2021", // = ngay tao don,
	"PURCHASING_ORG": "P001", // = SAP gửi data để lưu vào bảng warehouse_center_tower, thêm 2 cột thông tin purchasing org và company, dựa vào warehouse và center tower để lấy ra 2 trường thông tin này.
	"COMPANY_CODE": "P001", // = SAP gửi data để lưu vào bảng warehouse_center_tower, thêm 2 cột thông tin purchasing org và company, dựa vào warehouse và center tower để lấy ra 2 trường thông tin này.,
	"SALES_ORGANIZATION": "", //= de rong,
	"TEXT01": "Số 113 chi nhánh Nguyễn Tất Thành",// = dia chi,
	"DATA_STATUS": "C", // = "C”,
	"ITEM": [
		{
			"ROW_ID": "10", //= "id bang order_child_product_s1_receipt”,
			"MATERIAL": "250000015", //= ma sp SAP,
			"STORAGE_LOCATION": "P901", // = ma kho,
			"REQ_DELI_DATE": "08/12/2021", // = ngay hien tai,
			"BASE_UNIT": "KG", // = quy cach nho san pham, tim trong bang attribute_map_product,
			"ORDER_QUANTITY": "10000",// = so luong quy cach nho,
			"SALES_UNIT": "KG",// = quy cach lon san pham, tim trong bang attribute_map_product, neu khong co quy cach lon thi luu quy cach nho,
			"ORDER_QUANTITY_SU": "10000",// = so luong quy cach lon,neu khong co quy cach lon thi luu quy cach nho
			"PLANT": "P900",// = Ma TTPP,
			"BATCH": "",// = Ma lo
		}
	]
}

REPONSE:

{
    "PO_NUMBER": "AX12-6668",
    "SO": "4100000945",// => Luu lai SO,
    "INTEGRATIONS_STATUS": "S",
    "ERROR_NOTE": ""
}



