# SHOW TRẠNG THÁI SYCN ĐƠN Ở DANH SÁCH ĐƠN HÀNG

## 1. Status sync của đơn hàng

| Status | Diển giải SAP | Diển giải TMS
| -- | -- | -- |
| 0 | Chưa sync SAP | Chưa sync TMS |
| -1 | Thất bại SAP | Thất bại TMS |
| -2 | Thất bại SAP | Chưa sync TMS |
| -3 | Thành công SAP | Thất bại TMS |
| 1 | Thành công SAP | Chưa sync TMS |
| 2 | Thành công SAP | Thành công TMS |


## 2. Logic render trạng thái đơn

**Cột trạng thái SAP**

| Status | Diển giải | Mapping từ status sync đơn hàng |
| -- | -- | -- |
| 0 | chưa sync | 0 |
| -1 | thất bại | -1, -2 |
| 1 | thành công | -3, 1, 2 |
| -2 | chưa sync | _ |


**Cột trạng thái TMS**

| Status | Diển giải | Mapping từ status sync đơn hàng |
| -- | -- | -- |
| 0 | chưa sync | 0, -2, 1 |
| -1 | thất bại | -1, -3 |
| 1 | thành công | 2 |
| -2 | chưa sync | Loại đơn BKG |
