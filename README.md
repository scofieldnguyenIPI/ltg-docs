# LTG TECHNICAL DOCUMENTS

## Table of Contents

1. Đơn hàng
   1. C1 Bán hàng C2
      1. [Chặn bán hàng âm kho](./don_hang/c1_ban_hang_c2/Chặn%20bán%20hàng%20âm%20kho.md)
   2. C2 đặt hàng
      1. [Báo hạn mức khi C2 đặt hàng](./don_hang/c2_dat_hang/bao_han_muc_khi_c2_dat_hang.md)
      2. [Luồng đơn C2 2022](./don_hang/luong_don_c2/luong_don_c2_2022.md)
   3. [Đơn hàng nông dân](don_hang/don_hang_nong_dan/279_qly_mqh_nong_dan.md)
2. Tồn kho
   1. [Tồn kho C1](./ton_kho/ton_kho_thuong_c1.md)
   2. [Tồn kho C1 đầu ngày cuối ngày](./ton_kho/ton_kho_dau_ky_cuoi_ky.md)
3. Flow chính
   1. [Big Update 2022](./thiet_ke/Design%202022.md)
4. Tích hợp SAP
   1. Tồn kho
      1. [Tồn kho giữ hộ](./sync_sap/sync_ton_kho/sync_ton_kho_giu_ho_sap.md)
   2. [Show trạng thái sync đơn](./sync_sap/trang_thai_sync.md)
5. Tính năng TTPP
   1. [Danh sách đơn hàng TGH ở TTPP thường](./ttpp/329_ds_dh_tgh.md)
