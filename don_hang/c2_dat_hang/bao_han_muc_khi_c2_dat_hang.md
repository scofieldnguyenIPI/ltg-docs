# BÁO HẠN MỨC KHI C2 ĐẶT HÀNG

## 1. Nhu cầu
Show danh sách các hạn mức mà giỏ hàng vượt để tiến hành điều chỉnh

## 2. Thiết kế DB (Không có)

## 3. Logic

1. Client gửi lên danh sách sản phẩm + số lượng
2. Xét hạn mức tổng
   1. Vượt => Thông báo số tiền vượt cho client
   2. Không vượt => Xử lý tiếp
3. Chuẩn bị danh sách hạn mức bị vượt = []
4. Lấy toàn bộ hạn mức nhóm sản phẩm liên quan đến sản phẩm client gửi
   1. QUERY: 
      1. TABLE: user_inventory_limited -> user -> user_limited_group_children -> user_limited_group_children_detail -> product_group -> product_group_detail -> product 
      2. WHERE user_id, start_date & and_date, id sản phẩm trong list sản phẩm
   1. Lặp từng hạn mức đã tìm thấy
      1. Khả dụng = hạn mức - thực hiện
      2. So sánh với số lượng nhu cầu 
         1. Khả dụng < nhu cầu
            1. Tính số tiền vượt = Nhu cầu - Khả dụng
            2. Add (Tên hạn mức, số tiền vượt) vào danh sách hạn mức bị vượt
               1. Khác nhóm sản phẩm => Add vào danh sách
               2. Trùng nhóm sản phẩm => Chọn hạn mức có số tiền vượt lớn hơn, add hạn mức lớn hơn vào danh sách
         2. Khả dụng > nhu cầu
            1. Cho Pass
5. Trả về cho client thông báo vượt
   1. Code: Đặc biệt
   2. Message: Danh sách hạn mức bị vượt
6. Client show
```
Đơn hàng của bạn vượt hạn mức. Bao gồm:
- nhóm sản phẩm _ vượt __ vnđ
- nhóm sản phẩm _ vượt __ vnđ
- nhóm sản phẩm _ vượt __ vnđ
- nhóm sản phẩm _ vượt __ vnđ
Vui lòng giảm số lượng tương ứng để đặt hàng.
```
