# FLOW ĐẶT HÀNG CỦA C2 LÊN C1

## 1. Flow đặt hàng của C2

Flow cũ => sẽ bỏ

```mermaid
graph LR;
A[C2] -- xác nhận --> B[C1]
```

Flow mới 

```mermaid
graph LR;
A[C2] -- xác nhận --> B[NVBH] -- xác nhận --> C[CN] -- xác nhận --> D[C1]
```


## 2. Trạng thái đơn

Flow cũ => sẽ bỏ

```mermaid
graph LR;
O[Init]
A[Chờ xác nhận]
B[Đang giao]
C[Đã giao]
D[Hoàn thành]

O -- C2 tạo đơn --> A
A -- C1 xác nhận --> B
B -- C2 xác nhận nhận hàng --> C
C -- C2 nhập kho --> D
```

Flow mới

```mermaid
graph LR;
O[Init]
A[Chờ NVBH xác nhận]
A1[Chờ CN xác nhận]
A2[Chờ đại lý xác nhận]
B[Đang giao]
C[Đã giao]
D[Hoàn thành]

O -- C2 tạo đơn --> A
A -- NVBH xác nhận --> A1
A1 -- CN xác nhận --> A2
A2 -- C1 xác nhận --> B
B -- C2 xác nhận nhận hàng --> C
C -- C2 nhập kho --> D
```


## 3. Config flow theo chi nhánh

```
table name: agency
thêm cột: flow_type
flow_type = 1 => sử dụng flow cũ
flow_type = 2 => sử dụng flow mới
```

##  4. Thao tác của C2 trong flow

### C2 đặt đơn

- Tìm CN trực thuộc
  - Cột agency_id trong bảng user
- Tạo đơn hàng
  - id_nvbh: => Từ c2 => tuyến => NVBH
  - id_chinhanh => chi nhánh vừa tìm
  - id_c1 => show danh sách c1 cho c2 chọn => ghi nhận c1
  - Trạng thái
    - Nếu flow_type của chi nhánh = 1 => Trạng thái đơn tạo ra là "Chờ đại lý xác nhận"
    - Nếu flow_type của chi nhánh = 2 => Trạng thái đơn tạo ra là "Chờ NVBH xác nhận"


### C2 quản lí đơn

Show các trạng thái đơn:

- Chờ NVBH xác nhận
- Chờ CN xác nhận
- Chờ đại lý xác nhận
- Đang giao
- Đã giao


##  4. Thao tác của NVBH trong flow

### C2 đặt đơn

- Tìm CN trực thuộc
  - Cột agency_id trong bảng user
- Tạo đơn hàng
  - id_nvbh: => Từ c2 => tuyến => NVBH
  - id_chinhanh => chi nhánh vừa tìm
  - id_c1 => show danh sách c1 cho c2 chọn => ghi nhận c1
  - Trạng thái
    - Nếu flow_type của chi nhánh = 1 => Trạng thái đơn tạo ra là "Chờ đại lý xác nhận"
    - Nếu flow_type của chi nhánh = 2 => Trạng thái đơn tạo ra là "Chờ NVBH xác nhận"


### C2 quản lí đơn

Show các trạng thái đơn:

- Chờ NVBH xác nhận
- Chờ CN xác nhận
- Chờ đại lý xác nhận
- Đang giao
- Đã giao