# Luồng đơn C2 năm 2022

## I. Trạng thái đơn

1. C2 đặt đơn => NV chưa xác nhận (36)
2. NV xác nhận => CN chưa xác nhận (37)
3. NV từ chối => Đơn từ chối (12)
4. CN xác nhận => ĐLC1 chưa xác nhận (1)
5. CN từ chối => Đơn từ chối (12)

## II. Thao tác

### 1. C2 đặt đơn

=> Cần đảm bảo C2 được mapping với C1 chung chi nhánh

1. Lấy chi nhánh của C1 mapping với C2 (nếu nhiều thì chọn C1)
2. Tìm flow_type của chi nhánh
3. flow_type == 1 (flow cũ)
  1. Lưu thêm agency_id
  2. Tìm saleman_id theo tuyến => ghi saleman_id vào
  3. Status đơn = 1
4. flow_type == 2 (flow mới)
  1. Saleman đặt => status đơn = 37
  2. Đại lý đặt => status đơn = 36
  3. Lưu agency_id
  2. Tìm saleman_id theo tuyến => ghi saleman_id vào

### 2. C2 quản lí đơn

4 tab:

- Chờ xác nhận
  - ĐLC1 chưa xác nhận (1)
  - CN chưa xác nhận (37)
  - NVBH chưa xác nhận (36)
- Đang giao
  - Đang giao (5)
- Đã giao
  - Đã giao (6)
- Hoàn thành
  - Hoàn thành (9)

### 3. NVBH duyệt + hủy đơn

#### 1. Đổ danh sách đơn
Đổ đơn theo id saleman và trạng thái (order_child cột saleman_id)

4 tab:

- Chờ xác nhận
  - ĐLC1 chưa xác nhận (1)
  - CN chưa xác nhận (37)
  - NVBH chưa xác nhận (36)
- Đang giao (5)
- Đã giao (6)
- Hoàn thành (9)

#### 2. Hủy đơn

Input:

- danh sách đơn
- user
- saleman thao tác

Logic:

- Lặp danh sách đơn
  - Check đơn ở trạng thái 36 mới cho hủy
  - Tạo note: NVBH - ngày thao tác - note (set cứng note rỗng)
  - Cập nhật trạng thái thành 12.

#### 3. Xác nhận

Input:

- danh sách đơn
- user
- saleman thao tác

Logic:

- Lặp danh sách đơn
  - Check đơn ở trạng thái 36 mới cho xác nhận
  - Cập nhật trạng thái thành 37.

#### 4. Đặt đơn

Gọi qua API đặt đơn C2 với trường is_saleman = 1

### 4. CN xử lý đơn hàng C2

#### 1. Danh sách đơn

Đổ đơn theo id agency (order_child cột agency_id)

Đổ đơn toàn bộ trạng thái

#### 2. Xác nhận đơn

- Check trạng thái đơn là 37 mới cho xác nhận
- Xác nhận chuyển sang trạng thái 1.

#### 3. Hủy đơn

- Check trạng thái đơn là 37 mới cho xác nhận
- Note = Tên chi nhánh - Mã nhân viên - Thời gian - lý do
- Chuyển qua trạng thái 12
