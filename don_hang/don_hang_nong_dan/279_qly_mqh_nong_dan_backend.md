# Mối quan hệ nông dân

## I. Thiết kế Database
    Không thay đổi database
## II. Danh sách API

### 1. API chi tiết đơn hàng nông dân
### Chỉnh sửa thêm key trong response
    farmer_field_code : mã ruộng
    farmer_field_name : tên ruộng

### Logic
    Trong đơn hàng nông dân có trường lưu id của ruộng(farmer_field_id). Từ id ruộng sẽ lấy tên ruộng, mã ruộng
### SQL
    Tìm trong 'loctroi_integration'.'farmer_field' theo id ruộng. Lấy ra mã ruộng(code), tên ruộng(name)

### 2. API danh sách mối quan hệ nông dân
### Link
    http://61.28.235.133:10003/swagger-ui.html#/relationship-farmer-rest/getAllRelationshipFarmerUsingGET

### Request
```
search(string) : search theo mã nông dân, tên nông dân 
agency_ids(int[]): danh sách id chi nhánh
user_status(int): (2 tất cả, 1 hoạt động, 0 ngừng hoạt động )
user_assign_status(int): (2 tất cả, 1 đã gán, 0 chưa gán)
field_assign_status(int): (2 tất cả, 1 đã gán, 0 chưa gán)
farmer_type(int): (2 tất cả, 1: NRMS, 0 LTG)
farmer_status(int): (2 tất cả, 1 hoạt động, 0 ngừng hoạt động )
material_region(int): (2 tất cả, 1 trong, 0 ngoài)
page
size
```

### Response
```
{
  "code": 1,
  "data": {
    "list": [
      {
        "farmer_name": "Lê Thị Phượng (kiểm tra)", // tên nông dân
        "farmer_code": "FC134396906",   // mã nông dân
        "farmer_agency": "",    //chi nhánh nông dân
        "farmer_type": "nRMS",  // loại nông dân
        "farmer_status": "Hoạt động",   // trạng thái nông dân
        "farmer_phone": "0981986718", // SĐT
        "farmer_cmnd": "230615503", // CMND nông dân
        "farmer_cmnd_ngaycap": 1508086800000, // Ngày cấp CMND nông dân(milisecond)
        "farmer_ngaysinh": 524854800000, // Ngày sinh nông dân(milisecond)
        "farmer_noicap": "CA Gia Lai", // nơi cấp CMND nông dân
        "farmer_address": "Chư Don", // địa chỉ nông dân
        "farmer_city": "Gia Lai", // tỉnh của nông dân
        "farmer_district": "Chư Sê", // huyện của nông dân
        "farmer_ward": "TT Chư Sê", // xã của nông dân
        "saleman_name": "Phượng - check hệ thống ", // tên nhân viên
        "saleman_code": "J6625", // mã nhân viên
        "material_region": "Ngoài", // vùng nguyên liệu
        "field_name": "", // tên ruộng
        "field_code": "", // mã ruộng
        "field_status": "", // trạng thái ruộng
        "field_assign_status": "Chưa gán",
        "field_city": "", // tỉnh của ruộng
        "field_district": "", // huyện của ruộng
        "field_ward": "", // xã của ruộng
        "s1_name": "", // tên S1
        "s1_code": "", // mã đại lý S1
        "s1_agency": "", // chi nhánh S1
        "s1_status": "", // trạng thái S1
        "s1_assign_status": "Chưa gán", // trạng thái gán S1
        "s1_city": "", // tỉnh S1
        "s1_district": "", // huyện S1
        "s1_ward": "", // xã S1
        "s2_name": "", // tên S2
        "s2_code": "", // mã đại lý S2
        "s2_agency": "", // chi nhánh S2
        "s2_status": "", // trạng thái S2
        "s2_assign_status": "Chưa gán", // trạng thái gán S2
        "s2_city": "", //tỉnh S2
        "s2_district": "", // huyện S2
        "s2_ward": ""// xã S2
      },
    ],
    "page": 0,
    "size": 0,
    "total": 0
  },
  "error": "string",
  "error_message": {
    "en": "string",
    "vn": "string"
  }
}
```

### Logic
- Mối quan hệ nông dân - đại lý thông qua ruộng(trong ruộng có trường farmer_id,user_id,user_type).
- Xử lý key
    - Trạng thái gán ruộng
        - Đã gán : khi đại lý có ruộng
        - Chưa gán : đại lý chưa có ruộng
    - Trạng thái gán đại lý
        - Đã gán : nông dân có ruộng đã gán đại lý
        - Chưa gán : nông dân không có ruộng hoặc ruộng chưa gán đại lý
    - Vùng nguyên liệu:
        - Trong: nv salerep
        - Ngoài: nv khác salerep
- Khi lấy data mối quan hệ, các trường chi nhánh, tỉnh,huyện,xã đều là id. Xử lý lấy tên chi nhánh, tỉnh,huyện,xã theo id
### SQL
- Lấy danh sách mối quan hệ nông dân,ruộng,đại lý.
```
    Từ loctroi_integration.farmer
    join loctroi_integration.farmer_field
    join user
    join user_agency
    join agency_user_agency_detail
    join saleman
```

### API xuất excel mối quan hệ
### Link
    http://61.28.236.90:10003/swagger-ui.html#/export-excel-rest/exportRelationshipFarmerC1C2FieldUsingGET
### Request
```
search(string) : search theo mã nông dân, tên nông dân 
agency_ids(int[]): danh sách id chi nhánh
user_status(int): (2 tất cả, 1 hoạt động, 0 ngừng hoạt động )
user_assign_status(int): (2 tất cả, 1 đã gán, 0 chưa gán)
field_assign_status(int): (2 tất cả, 1 đã gán, 0 chưa gán)
farmer_type(int): (2 tất cả, 1: NRMS, 0 LTG)
farmer_status(int): (2 tất cả, 1 hoạt động, 0 ngừng hoạt động )
material_region(int): (2 tất cả, 1 trong, 0 ngoài)
```
### Danh sách cột
```
STT, Mã nông dân, Tên nông dân, Tên chi nhánh, Loại nông dân, Trạng thái nông dân, Số điện thoại, Số CMND/CCCD, Ngày cấp, Ngày sinh, Nơi cấp, 
Địa chỉ, Tỉnh/thành, Quận/huyện, Phường/xã, Tên nhân viên, Mã nhân viên, Vùng nguyên liệu, 
Tên đại lý C1, Mã đại lý C1, Trạng thái đại lý C1, Chi nhánh, Tỉnh/thành, Quận/huyện, Phường/xã, 
Trạng thái gán DLC1, Tên đại lý C2, Mã đại lý C2, Trạng thái đại lý C2, Chi nhánh, 
Tỉnh/thành, Quận/huyện, Phường/xã, Trạng thái gán DLC2, Tên Ruộng, Mã Ruộng, Trạng thái Ruộng, Tỉnh/thành, 
Quận/huyện, Phường/xã, Trạng thái gán Ruộng
```
