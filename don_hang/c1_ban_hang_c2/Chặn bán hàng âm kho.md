# C1 chặn bán hàng âm kho
```
Kiểm tra tồn kho thường và chặn không cho C1 bán hàng cho C2 gây âm kho
```

### 1. Tổ chức DB

Bổ sung field ```is_negative_warehouse``` trong bảng user_agency

```
Name: is_negative_warehouse
Type: Int
Length: 11
Not Null
Comment: 0: không được phép bán âm kho; 1: Được phép bán âm kho
```
```
is_negative_warehouse = 1 => Được phép bán âm kho

is_negative_warehouse = 0 => Không được phép bán âm kho
```

### 2. API kiểm tra tồn kho trước khi giao hàng

**Luồng xử lý**
- Client gửi danh sách sản phẩm và số lượng từ nhiều đơn hàng (do C1 có thể giao nhiều đơn của C2 cùng lúc)

```
danh sách đơn hàng
Đơn 1:
- A 10
- B 10
Đơn 2:
- B 10
- C 20
```

- Tính số lượng nhu cầu, sum số lượng theo mã sản phẩm

```
Nhu cầu
A: 10
B: 20
C: 20
```

- Kiểm tra số lượng tồn kho của các sản phẩm nhu cầu
	- Kiểm tra trong bảng ```warehouse_user_agency```
		- Filter: Theo danh sách mã sản phẩm, mã đại lý, type = 2 (kho thường)
	- Nếu sản phẩm cần kiểm tra không tồn tại trong tồn kho đại lý => Số lượng tồn kho sản phẩm = 0

```
Tồn kho
A: 10
B: 25
C: 15
D: 0 (D không tồn tại trong tồn kho đại lý => SL = 0)
```

- Lấy danh sách đơn hàng ở trạng thái đang giao (5) và đã giao (6)
	- Filter: Theo danh sách mã sản phẩm (danh sách sản phẩm nhu cầu), mã đại lý, type = 2 (kho thường)

```
Danh sách đơn hàng đang giao và đã giao
ĐH1 - A - 10
ĐH1 - E - 10
ĐH2 - E - 10
ĐH2 - A - 20
ĐH2 - B - 30
```

- Sum số lượng theo mã sản phẩm

```
Số lượng ngậm đang giao và đã giao
A 30
B 30
E 20
```

- Tính số lượng khả dụng đại lý có thể bán: Tồn kho - Số lượng ngậm đang giao và đã giao

```
khả dụng
A = 10 - 30 = -20
B = 25 - 30 = -5
C = 15 - 0 = 15
D = 0 - 0 = 0
```

- Loop sản phẩm nhu cầu, tiến hành kiểm tra các sản phẩm có thể giao = khả dụng - nhu cầu


```
khả dụng
A = -20
B = -5
C = 15
D = 0

Nhu cầu
A: 10
B: 20
C: 20

kiểm tra
A = -20 - 10 = -30
B = -5 - 20 = -25
C = 15 - 20 = -5
```

- Trả thông tin về cho client
	- Thông tin tồn kho ( danh sách tồn kho khả dụng của các sản phẩm âm)
	- Đại lý có bị chặn bán âm kho hay không

```
Trả về client
Thông tin tồn kho
A = -20
B = -5
C = 15

Đại lý chặn hay không chặn âm kho (0 hoặc 1)
```

- Client xử lý
	- Nếu đại lý bị chặn âm kho
		- Có thông tin tồn kho => Show popup báo tồn kho nhưng không có nút xác nhận đơn
		- Không có thông tin tồn kho => Xác nhận giao đơn
	- Nếu đại lý không bị chặn âm kho => Show popup báo tồn kho và có nút xác nhận đơn
