# Tồn kho thường C1

Khi các loại đơn này hoàn thành sẽ ảnh hưởng đến tồn kho thường của C1

|     Loại đơn       | Ảnh hưởng đến tồn kho  |
|--------------------|-------------|
|     Đơn thường     |     Nhập       |
|     Đơn bán C2     |     Xuất       |
|     Đơn trả giữ hộ |     Nhập       |
| Đơn đặt ký gửi     | Nhập           |
| Đơn bán ND         | Xuất           |
| Đơn trả ký gửi     | Xuất           |