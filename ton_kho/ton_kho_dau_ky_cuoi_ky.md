# Tồn kho đầu ngày cuối ngày

## 1. Các trường thông tin quan trọng

- Tồn đầu ngày = Tồn cuối ngày hôm trước
- Nhập trong ngày = Query số lượng nhập trong ngày theo [Đại lý, Sản phẩm]
- Xuất trong ngày = Query số lượng xuất trong ngày theo [Đại lý, Sản phẩm]
- Tồn cuối ngày = Tồn đầu ngày + Nhập - Xuất + Số lượng chênh lệch
- Kiểm kê = 0 (mặc định)
- Chênh lệch = 0 (mặc định)

## 2. Xác nhận kiểm kê

- Khi xác nhận kiểm kê
  - Lấy ngày tạo kiểm kê
  - Kiểm tra có tồn tại kiểm kê chung [Đại lý, Sản phẩm] trước ngày tạo kiểm kê (Trường hợp kiểm kê nhiều lần khác ngày)
    - Có tồn tại => Thông báo Admin hoặc NV cần phải xác nhận lần kiểm kê trước đó trước khi xác nhận lần kiểm kê này
    - Không tồn tại => Cho pass
- Xác nhận dòng kiểm kê thành công
  - Tìm ngày tạo kiểm kê
  - Tìm dòng tồn kho đầu cuối kỳ có trùng [Đại lý, Sản phẩm, Ngày = Ngày tạo kiểm kê]
  - Cập nhật số lượng kiểm kê vào cột kiểm kê
    - Nếu cột kiểm kê hiện tại = 0 => cột kiểm kê = số lượng trong phiếu kiểm kê
    - Nếu cột kiểm kê hiên tại != 0 => cột kiểm kê = cột kiểm kê + số lượng trong phiếu kiểm kê
  - Tính số lượng chênh lệch
    - Chênh lệch = Kiểm kê - cuối ngày
    - Cập nhật lại số lượng cuối ngày = Tồn đầu ngày + Nhập - Xuất + Số lượng chênh lệch
  - Tính lại số lượng đầu và cuối của các ngày liên quan
    - Tìm ra dòng đầu kỳ cuối kỳ của ngày kế tiếp có trùng [Đại lý, sản phẩm]
      - Cập nhật
        - Tồn đầu ngày = Tồn cuối ngày hôm trước
        - Tồn cuối ngày = Tồn đầu ngày + Nhập - Xuất + Số lượng chênh lệch
    - Kết thúc khi không còn tìm được dòng nào