# BUILD 343. Sync Đơn hàng qua SAP TMS

http://redmine.ipicorp.co/issues/46469

#### Author: Phạm Hồng Ân - FE

## I/ Nhu cầu

- Sync đơn hàng qua SAP TMS

## II/ Giải pháp

- Tạo page mới __Sync Đơn hàng qua SAP TMS__
- Cách truy cập: submenu → Sync Đơn hàng qua SAP TMS
- Path: `sync/order`
- API sử dụng: `order_s1/sync_order_sap_tms`
- params:

| STT | params | defined_key |
|:---:|:------:|:-----------:|
|  1  | order_code |             |
|  2  | sync_status| default: 0  |