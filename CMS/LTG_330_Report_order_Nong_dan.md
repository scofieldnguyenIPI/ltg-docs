# BUILD 330. Báo cáo chi tiết đơn hàng nông dân

http://redmine.ipicorp.co/issues/46012

#### Author: Phạm Hồng Ân - FE

## I/ Nhu cầu

- Tool ADMIN: Danh sách Báo cáo chi tiết đơn hàng nông dân
- Tool CHI NHÁNH: Xuất báo cáo chi tiết đơn hàng nông dân

## II/ Giải pháp

### 1/ ADMIN
- Tạo mới page Báo cáo Chi tiết đơn hàng nông dân

1. Cách truy cập: subMenu → Báo cáo tổng hợp → chọn Báo cáo CTDH Nông dân
2. Path: `/export/order/farmer`
3. Title Page: `Báo cáo Chi tiết Đơn hàng Nông dân`
4. API: `sumary_report/report_detail_order_farmer`
5. Params

| STT |       params        |                          note                          |
|:---:|:-------------------:|:------------------------------------------------------:|
|  1  |     start_date      |                      Ngày bắt đầu                      |
|  2  |      end_date       |                     Ngày kết thúc                      |
|  3  |       search        | Tìm kiếm theo tên ND, mã ND, mã đơn hàng, tên sản phẩm |
|  4  |        page         |                                                        |
|  5  |        size         |                      default = 10                      | 
|  6  |     agency_ids      |        Chi nhánh: agency/get_all_agency_active         |
|  7  | business_region_ids |      Vùng: business_region/get_all (params: key)       |
|  8  |     company_id      |        Ngành hàng: product/get_cate_by_company         |

6. Thông tin columns

| STT |     Tên cột      |        key         |  type  |       note       |
|:---:|:----------------:|:------------------:|:------:|:----------------:|
|  1  |       STT        |       `stt`        |  int   |                  | 
|  2  |   Mã đơn hàng    |    `order_code`    | string |                  |
|  3  |  Ngày đặt hàng   |   `create_date`    |  int   | #DD#/#MM#/#YYYY# |
|  4  |   Mã nông dân    |       `mand`       | string |                  |
|  5  |   Tên nông dân   |      `tennd`       | string |                  |
|  6  |    Mã đại lý     |     `s2_code`      |  int   |                  |
|  7  |    Tên đại lý    |     `s2_name`      | string |                  |
|  8  | Vùng kinh doanh  |  `vungKinhDoanh`   | string |                  |
|  9  |    Chi nhánh     |     `chiNhanh`     |  int   |                  |
| 10  |   Mã nhân viên   |    `sale_code`     |  int   |                  |
| 11  |  Tên nhân viên   |    `sale_name`     | string |                  |
| 12  |   Mã sản phẩm    |   `product_code`   | string |                  |
| 13  |   Tên sản phẩm   |       `name`       | string |                  |
| 14  |   Nhóm hàng 1    |    `type_name`     | string |                  |
| 15  |   Nhóm hàng 2    |    `cate_name`     | string |                  |
| 16  |     Quy cách     |    `attr_name`     | string |                  |
| 17  |   Số lượng đặt   | `product_quantity` |  int   |                  |
| 18  |     Đơn giá      |  `product_price`   |  int   |                  |
| 19  |    Thành tiền    |  `total_money`     |  int   |                  |

### 2. Xuất file ADMIN

1. Cách truy cập: Tại page __Báo cáo Chi tiết Đơn hàng Nông dân__ → click chọn __Xuất file__
2. API: `adminservice/export_excel/export_detail_order_farmer`
3. Params: giống api danh sách

### 3. Xuất báo cáo ở tool CHI NHÁNH

1. Cách truy cập: submenu __Báo cáo tổng hợp__ → click chọn __Báo cáo tổng hợp__ → redirect page __Trích xuất báo cáo tổng hợp__
2. API: `/adminservice/export_excel/cn/export_detail_order_farmer`
3. Params: `start_date, end_date, search`
4. Note: User chọn checkbox __Báo cáo chi tiết đơn hàng nông dân__  → chọn ngày bắt đầu, kết thúc → chọn __Xuất báo cáo__ → export được file báo cáo chi tiết đơn hàng nông dân trên tool chi nhánh