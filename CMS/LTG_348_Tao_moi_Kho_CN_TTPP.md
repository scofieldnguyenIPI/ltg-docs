# BUILD 348. Tạo mới Chi nhánh - TTPP - Kho

http://redmine.ipicorp.co/issues/46950

#### Author: Phạm Hồng Ân - FE

## I/ Nhu cầu

- Ở CMS ADMIN, cho phép Tạo mới Kho, chi nhánh, TTPP
- Phát triển thêm UI/UX trang danh sách hiện có
- Cho phép chỉnh sửa, xóa
- Ẩn các trường thông tin yêu câù
- Update tính năng tạo mới từ flow cũ trước đây

## II/ Giải pháp

### 1/ Chi nhánh

#### 1.1/ Danh sách Chi nhánh

1. Cách truy cập: subMenu → Hệ thống kênh phân phối → chọn Chi nhánh
2. Path: `/branchpage`
3. Title Page: `Chi nhánh`
4. API: `agency/get_list_agency`
5. Params

| STT | params |         defined_key         | note |
|:---:|:------:|:---------------------------:|:----:|
|  1  | search | | Tìm kiếm theo Tên chi nhánh, mã chi nhánh |
|  2  | flow_type| 1: Flow 2022 - 2: Flow 2022 |
|  3  | page|                             |

6. Thông tin columns

|STT|       Tên cột       |key|type|note|
|:---------------:|:-------------------:|:---------------:|:----------------:|:----------------:|
|1|         STT         |`stt`|int|
|2|         ID          |`id`|string|
|3|    Tên chi nhánh    |`name`|string|
|4|    Mã chi nhánh     |`code`|string|
|5|       Địa chỉ       |`address`|string|
|6|   Địa bàn quản lý   |`region_name`|string|
|7|       Số ĐLC1       |`count_s1`|int|
|8|    Số lượng TTPP    |`count_center_tower`|int|
|9|        Flow         |`flow_type`|int|

#### 1.2/ Chi tiết Chi nhánh

1. Truy cập: Tại page __Chi nhánh__ → click chọn 1 chi nhánh → redirect đến page __Chi tiết chi nhánh__
2. Path: ```/detailbranch/:id```
3. API sử dụng: ```agency/get_detail_agency```
4. Thông tin chi tiết

![Chi tiết Chi nhánh!](DeatailOfBranch.png "Detail Branch")

#### 1.3/ Xóa Chi nhánh

1. API: `agency/update_status`

```angular2html
Request: {ids: [],status: 0,}
```

#### 1.4/ Xuất file Chi nhánh

1. API: `adminservice/export_excel/export_agency`
2. Params: `keySearch, flow_type`
3. Tên file: `Danh_sach_chi_nhanh.csv`

#### 1.5/ Chuyển flow Chi nhánh

1. API: `agency/update_flow_type_agency`

```angular2html
Request:
{
        ids: [],
        flow_type: 0,
        }
```

2. Rules: Chỉ được phép chọn Chuyển flow các Chi nhánh có cùng flow hiện tại.

#### 1.6/ Tạo mới Chi nhánh

1. Truy cập: Tại page __Chi nhánh__ → click __Tạo mới__ → redirect đến page __Tạo mới chi nhánh__
2. Path: ```/create/branch/0```

![Tạo mới Chi nhánh!](CreateBranch.png "Create Branch")

#### 1.7/ Chỉnh sửa Chi nhánh

1. Truy cập: Tại page __Chi tiết chi nhánh__ → click __Chỉnh sửa__ → redirect đến page __Chỉnh sửa chi nhánh__
2. Path: ```/branch/edit/mapping/:id```
3. API sử dụng: ```agency/update```

```angular2html
Request:
   {
      agency_id: 0,
      address: "",
      code_region: "",
      agency_name: "",
      region_id: 0,
      center_tower_mapping_list: []
   }
```

4. Rules: Không được chỉnh sửa `Mã chi nhánh`

### 2/ Trung tâm phân phối

#### 2.1/ Danh sách Trung tâm phân phối

1. Cách truy cập: subMenu → Hệ thống kênh phân phối → chọn Trung tâm phân phối
2. Path: `/distributionPage`
3. Title Page: `Trung tâm phân phối`
4. API: `center_tower/get_all`
5. Params:
----
| STT | params |            defined_key            | note |
|:---:|:------:|:---------------------------------:|:----:|
|  1  | key |                                   |Tìm kiếm theo Tên TTPP, mã TTPP |
|  2  | city_id|                                   |     int     |
|  3  | agency_id|                                   |int|
|  4  | status|            default: 1             ||
|  5  | center_tower_type| 1: TTPP thường - 2: TTPP Logistic |int
|  6  | page|                                   |

6. Thông tin columns

|STT|         Tên cột          |key|  type  |note|
|:---------------:|:------------------------:|:---------------:|:------:|:----------------:|
|1|           STT            |`stt`|  int   |
|2|         Tên TTPP         |`name`| string |
|3|         Mã TTPP          |`code`| string |
|4|           Loại           |`center_tower_name`| string |
|5|         Địa chỉ          |`address`| string |
|6|      CN trực thuộc       |`agency_count`|  int   |
|7| TTPP Logistic trực thuộc |`ttpp_logistic_count`|  int   |
|8|      Kho trực thuộc      |`warehouse_count`|  int   |
|9|       TTPP mapping       |`countTtpp`|  int   |

#### 2.2/ Chi tiết Trung tâm phân phối

1. Truy cập: Tại page __Trung tâm phân phối__ → click chọn 1 TTPP → redirect đến page __Chi tiết Trung tâm phân phối__
2. Path: ```/detailDistribution/:id```
3. API sử dụng: ```center_tower/detail```
4. Thông tin chi tiết

![Chi tiết Trung tâm phân phối!](DetailDistribution.png "Detail Distribute")

#### 2.3/ Xóa Trung tâm phân phối

1. API: `center_tower/update_status`

```angular2html
Request:
{
id: [],
status: 0
}
```

#### 2.4/ Xuất file Trung tâm phân phối

1. API: `adminservice/export_excel/export_center_tower`
2. Params: `city_id, key, agency_id, status, center_tower_type`
3. Tên file: `Danh_sach_TTPP.csv`

#### 2.5/ Tạo mới Trung tâm phân phối

1. Truy cập: Tại page __Trung tâm phân phối__ → click __Tạo mới__ → redirect đến page __Tạo mới Trung tâm phân phối__
2. Path: ```/create/branch/0```

![Tạo mới Trung tâm phân phối Thường/Logistic!](CreateDistribution.png "Create Distribution")

#### 2.6/ Chỉnh sửa Trung tâm phân phối

1. Truy cập: Tại page __Chi tiết Trung tâm phân phối__ → click __Chỉnh sửa__ → redirect đến page __Chỉnh sửa Trung tâm
   phân phối Logistic__ hoặc __Chỉnh sửa Trung tâm phân phối__
2. Path: ```/distribute/edit/mapping/:id```
3. API sử dụng: ```center_tower/update_center_tower```

```angular2html
Request:
{
center_tower_id: 0,
address: "",
name: "",
purchasing_org: "",
agency_mapping_list: [],
center_tower_logistic_mapping_list: [],
center_tower_mapping_list: []
}
```

4. Rules: Không được chỉnh sửa `Mã TTPP`

### 3/Kho

#### 3.1/ Danh sách Kho

1. Cách truy cập: subMenu → Hệ thống kênh phân phối → chọn Kho
2. Path: `/warehouse`
3. Title Page: `Kho`
4. API: `warehouse/get_all`
5. Params

| STT | params |                 defined_key                 | note |
|:---:|:------:|:-------------------------------------------:|:----:|
|  1  | key |                                             |Tìm kiếm theo Tên TTPP, mã TTPP |
|  2  | warehouse_type| 1: Kho thường, 2: Kho giữ hộ, 3: Kho ký gửi |          |
|  3  | status|               0: Ẩn, 1: Hiện                |||
|  4  | page|                                             |

6. Thông tin columns

|STT|         Tên cột          |key|  type  |note|
|:---------------:|:------------------------:|:---------------:|:------:|:----------------:|
|1|           STT            |`stt`|  int   |
|2|           TTPP           |`fullnameTTPP`| string |
|3|          Mã Kho          |`code_warehouse`| string |
|4|         Tên Kho          |`name_warehouse`| string |
|5|         Loại Kho         |`warehouse_type_name`| string |
|6|        Trạng thái        |`status_name`| string |

#### 3.2/ Chi tiết Kho

1. Truy cập: Tại page __Kho__ → click chọn 1 Kho → redirect đến page __Chi tiết Kho__
2. Path: ```/warehouse/:id```
3. API sử dụng: ```warehouse/wh_detail```
4. Thông tin chi tiết

| STT | Tên thông tin |key|  type  |note|
|:---:|:-------------:|:---------------:|:------:|:----------------:|
|  1  |    Mã kho     |`code_warehouse`|  int   |
|  2  |     TTPP      |`fullnameTTPP`| string |
|  3  |    Tên Kho    |`name_warehouse`| string |
|  4  |   Loại Kho    |`warehouse_type_name`| string |
|  5  |  Trạng thái   |`status_name`| string |


#### 3.3/ Xóa Kho

1. API: `warehouse/update_status`

```angular2html
   Request:
      {
         id: [],
         status: 0
      }
```

#### 3.4/ Xuất file Kho

1. API: `adminservice/export_excel/export_warehouse`
2. Params: `search, warehouse_type, status`
3. Tên file: `Danh_sach_Kho_admin.csv`

#### 3.5/ Tạo mới Kho

1. Truy cập: Tại page __Kho__ → click __Tạo mới__ → redirect đến page __Tạo mới Kho__
2. Path: ```/create/warehouse/0```

![Tạo mới Kho!](CreateWarehouse.png "Create Warehouse")

#### 3.6/ Chỉnh sửa Kho

1. Truy cập: Tại page __Chi tiết Kho__ → click __Chỉnh sửa__ → redirect đến page __Chỉnh sửa Kho__
2. Path: ```/edit/warehouse/:id```
3. API sử dụng: ```warehouse/update_wh```

```angular2html
Request:
   {
      center_tower_id: 0,
      code_warehouse: "",
      name_warehouse: "",
      status: 0,
      warehouse_type: 0,
      warehouse_id: 0,
   }
```

4. Rules: Không được chỉnh sửa `Mã Kho`

