# BUILD 345. API Nông dân

http://redmine.ipicorp.co/issues/46722

#### Author: Phạm Hồng Ân - FE

## I/ Nhu cầu

- Viết các API chỉnh sửa thông tin nông dân
- API đẩy thông tin chỉnh sửa từ nRMS qua LTG
- API đẩy thông tin chỉnh sửa từ LTG qua nRMS
- Sync thông tin ruộng
- Chỉ chỉnh sửa loại nông dân tạo từ nRMS, không chỉnh chỉnh sửa nông dân tạo từ LTG

## II/ Giải pháp

### 1/ Danh sách nông dân

1. Cách truy cập: subMenu → Nông dân & Ruộng → chọn Danh sách nông dân
2. Path: `/list/new/farmer`
3. Title Page: `Danh Sách Nông Dân`
4. API: `event/get_all_farmer`
5. Params

| STT |     params     |                                              defined_key                                              |                        note                        |
|:---:|:--------------:|:-----------------------------------------------------------------------------------------------------:|:--------------------------------------------------:|
|  1  |     status     | Tất cả: 0 - Chưa sync qua LTG: -2 - Đã sync qua LTG: -1 - Đã sync qua nRMS: 1 - Sync LTG thất bại: -3 |                 Trạng thái đồng bộ                 |
|  2  |    isActive    |                             1: Tất cả - 1: Hoạt động - 0: Ngừng hoạt động                             |                Trạng thái nông dân                 |
|  3  |     search     |                                                                                                       | Tìm kiếm theo mã, tên nông dân, SĐT, số CMND/CCCD  |
|  4  |      page      |                                                                                                       |
|  5  |      size      |                                                                                                       |                    default = 10                    | 
|  6  |   agency_id    |                                                                                                       |                     Chi nhánh                      |
|  7  |    city_id     |                                                                                                       |
|  8  |  district_id   |                                                                                                       |
|  9  |    ward_id     |                                                                                                       |
| 10  |   farmerType   |                                     Tất cả: -1 - LTG: 0 - nRMS: 1                                     |                   Loại nông dân                    |
| 11  | regionMaterial |                                    Tất cả: 0 - Trong: 3 - Ngoài: 2                                    |                  Vùng nguyên liệu                  | 

6. Thông tin columns

| STT |       Tên cột       |        key        |  type  |                     note                     |
|:---:|:-------------------:|:-----------------:|:------:|:--------------------------------------------:|
|  1  |         STT         |       `stt`       |  int   |                                              | 
|  2  |     Mã nông dân     |    `maNongDan`    | string |                                              |
|  3  |    Tên nông dân     |   `tenNongDan`    | string |                                              |
|  4  |      Chi nhánh      |   `agency_name`   | string |                                              |
|  5  |    Loại nông dân    |   `farmer_type`   | string |                                              |
|  6  | Trạng thái nông dân |    `isActive`     |  int   | isActive === 1 ? Hoạt động : Ngừng hoạt động |
|  7  |    Số điện thoại    |       `SDT`       | string |                                              |
|  8  |    Số CMND/CCCD     |      `CMND`       | string |                                              |
|  9  |      Ngày cấp       |   `ngayCapCMND`   |  int   |               #DD#/#MM#/#YYYY#               |
| 10  |      Ngày sinh      |    `ngaySinh`     |  int   |               #DD#/#MM#/#YYYY#               |
| 11  |       Nơi cấp       |   `noiCapCMND`    | string |                                              |
| 12  |       Địa chỉ       |     `diachi`      | string |                                              |
| 13  |     Tỉnh/Thành      |    `tinh`         | string |                                              |
| 14  |    Mã Tỉnh/Thành    |    `city_code`    | string |                                              |
| 15  |     Quận/Huyện      |      `huyen`      | string |                                              |
| 16  |    Mã Quận/Huyện    |  `district_code`  | string |                                              |
| 17  |      Phường/Xã      |       `xa`        | string |                                              |
| 18  |    Mã Phường/Xã     |    `ward_code`    | string |                                              |
| 19  |    Tên nhân viên    |    `sale_name`    | string |                                              |
| 20  |    Mã Nhân viên     |    `sale_code`    | string |                                              |
| 21  |  Vùng nguyên liệu   | `region_material` | string |                                              |
| 22  |   Trạng thái sync   |   `statusName`    | string |                                              |
| 23  |   Lý do Sync lỗi    |    `errorNote`    | string |                                              |

### 2. Đồng bộ tất cả

- Là tính năng cho phép đồng bộ nhiều nông dân cùng một lúc
- Chi được phép đồng bộ các nông dân có status = -2 (Chưa sync qua LTG)
- API sử dụng: `event/sync_all_farmer`

  ```Request: {ids: []}```

### 3. Chi tiết nông dân

1. Truy cập: Tại page __Danh Sách Nông Dân__ → click chọn 1 nông dân → redirect đến page __Chi Tiết Nông Dân__
2. Path: ```/manage/new/farmer/:id```
3. API sử dụng: ```event/get_detail_farmer```
4. params: id của nông dân
5. Thông tin chi tiết

![Chi tiết Nông dân!](DetailFarmer.png "Detail Farmer")

### 4. Lịch sử chỉnh sửa nông dân

1. Truy cập: Tại page __Chi Tiết Nông Dân__ → click Lịch sử → redirect đến page __Lịch sử chỉnh sửa nông dân__
2. Path: ```/history/farmer/:id```
3. API sử dụng: ``event/get_list_history_update_farmer```
4. params:

| STT |      params      | defined_key |     note     |
|:---:|:----------------:|:-----------:|:------------:|
|  1  |   startDate      |             |              |
|  2  |     endDate      |             |              |
|  3  |     farmerId     |             |              |
|  4  |       page       |             |
|  5  |       size       |             | default = 10 | 

5. Thông tin columns

| STT |       Tên cột       |        key         |   type   |               note               |
|:---:|:-------------------:|:------------------:|:--------:|:--------------------------------:|
|  1  |         STT         |       `stt`        |   int    |                                  | 
|  2  |     Mã nông dân     |       `mand`       |  string  |                                  |
|  3  |    Tên nông dân     |      `tennd`       |  string  |                                  |
|  4  |    Số điện thoại    |       `sdt`        |  string  |                                  |
|  5  |      CMND/CCCD      |     `cmnd`         | string   |                                  |
|  6  |      Ngày sinh      |     `ngaySinh`     |   int    | #hh#:#mm#:#ss# #DD#/#MM#/#YYYY#  |
|  7  |     Tỉnh/Thành      |       `tinh`       |  string  |                                  |
|  8  |     Quận/Huyện      |      `huyen`       |  string  |                                  |
|  9  |      Phường/Xã      |        `xa`        |   int    |                                  |
| 10  | Trạng thái nông dân | `trangThaiNongDan` |  string  |                                  |
| 11  | Ngày giờ chỉnh sửa  |   `ngayCapNhat`    |   int    | #hh#:#mm#:#ss# #DD#/#MM#/#YYYY#  |
| 12  |     Trạng thái      |    `trangThai`     |  string  |                                  |
| 13  |  Account thao tác   |   `nguoiCapNhat`   |  string  |                                  |

### 5. Chỉnh sửa Nông Dân

1. Truy cập: Tại page __Chi Tiết Nông Dân__ → click Chỉnh sửa → redirect đến page __Chỉnh sửa Nông Dân__
2. Path: ```/edit/farmer/:id```
3. API sử dụng: `event/update_farmer`

Requets:
```angular2html
  {
    city_id: 0,
    cmnd: "",
    dia_chi: "",
    district_id: 0,
    farmer_id: 0,
    mand: "",
    noi_cap: "",
    sdt: "",
    status: 0,
    tennd: "",
    ward_id: 0,
    ngay_cap: 0,
    ngay_sinh: 0,
  }
```

4. Rules: Không được phép chỉnh sửa các thông tin sau:
- Mã nông dân
- Người tạo
- Cấp người tạo
- Mã người tạo
- Thời gian tạo nông dân
- Ngày tạo
- Trạng thái đồng bộ