# Thiết kế 2022

## I. Nhu cầu

- Thêm 1 đối tượng ông quản lý Vùng (super chi nhánh) chịu trách nhiệm duyệt đơn hàng
  - Là 1 ông chi nhánh có quyền duyệt đơn
- Tách TTPP theo vùng chiến lược
  - Chi nhánh liên kết với TTPP theo yếu tố ngành (1)
  - Đơn hàng lên Chi nhánh, khi được Vùng duyệt sẽ đẩy xuống TTPP theo Ngành hàng dựa theo tiếu chí (1)
- Đưa luồng trả giữ hộ về SAP
  - Đơn trả giữ hộ cần tách đơn theo ngành
  - Đẩy đơn giữ hộ SAP và ghi nhận dữ liệu xuất kho theo TTPP Logistic
- Thêm TTPP Logistic cho đơn hàng giữ hộ
  - Thêm bảng data cho TTPP Logistic
  - Thêm bảng data mapping TTPP LTG - TTPP Logistic
- Công nợ của Đại lý được phân theo từng Ngành
  - Hạn mức công nợ tổng
  - Công nợ chi tiết

## II. Thiết kế

### 1. Thêm 1 đối tượng ông quản lý Vùng (super chi nhánh) chịu trách nhiệm duyệt đơn hàng

- Căn bản vùng vẫn là chi nhánh, với mỗi account vùng sẽ phân quyền data để thao tác được trên nhiều chi nhánh
- Đối với nhu cầu này sẽ sử dụng tính năng phân quyền data có sẵn và hệ thống không cần chỉnh sửa gì

### 2. Luồng CN duyệt đơn hàng và submit đơn lên TTPP, TTPP logsitic

#### Nhu cầu

Thiết kế này giải quyết được 2 nhu cầu

```
- Tách TTPP theo vùng chiến lược
  - Chi nhánh liên kết với TTPP theo yếu tố ngành (1)
  - Đơn hàng lên Chi nhánh, khi được Vùng duyệt sẽ đẩy xuống TTPP theo Ngành hàng dựa theo tiếu chí (1)
```

```
- Thêm TTPP Logistic cho đơn hàng giữ hộ
  - Thêm bảng data cho TTPP Logistic
  - Thêm bảng data mapping TTPP LTG - TTPP Logistic
```

#### Cập nhật DB

Mapping giữa CN-TTPP

```
Thêm cột ngành vào Table mapping CN-TTPP (agency_center_tower_detail: company_id)
```

Mapping giữa TTPP thường - TTPP logistic

```
Bổ sung cột phân loại center_tower_type trong bảng center_tower.
1 => TTPP thường
2 => TTPP logsitic

Tạo bảng mapping TTPP thường & logistic
center_tower_id => id TTPP thường
order_type => loại đơn hàng
company_id => id nghành
logistic_id => id TTPP logistic
```

#### Logic

**Logic khi CN submit đơn hàng trong trường hợp đơn hàng mới đặt và chưa từng đẩy lên TTPP**

Đảm bảo TTPP nhận được đơn là TTPP logistic đối với đơn trả giữ hộ

Các thông báo lỗi

- (1) CN chưa được map với TTPP nào theo nghành {tên nghành}. Vui lòng thử lại sau.
- (2) TTPP {tên TTPP thường} chưa được map với TTPP logistic nào. Vui lòng thử lại sau.


Logic

- CN submit đơn hàng mới
- Lấy nghành hàng của đơn
- Lấy ra danh sách các TTPP được mapping với CN theo nghành
- Nếu
  - Có 1 TTPP => sử dụng TTPP này
  - Có > 1 TTPP => Show popup cho CN chọn => CN chọn TTPP => sử dụng TTPP này
  - Không có TTPP nào => show popup báo lỗi (1)
- Sau khi đã lấy ra được TTPP thường, kiểm tra trong bảng mapping giữa TTPP thường và logistic
  - Từ id TTPP thường lấy ra được các loại đơn cần đẩy lên Logistic
  - Nếu loại đơn của đơn hàng đang xét nằm trong danh sách các loại đơn cần đẩy lên TTPP logistic:
    - Từ id TTPP thường, loại đơn và nghành tìm ra được id TTPP logistic
      - Nếu không có TTPP logsitic được mapping tương ứng => Báo lỗi (2)
      - Nếu có TTPP logistic => Ghi nhận id TTPP logistic vào cột `center_tower` của đơn
  - Nếu loại đơn của đơn hàng không nằm trong danh sách các đơn cần đẩy lên TTPP logistic:
    - Ghi nhận id TTPP thường vào cột `center_tower` của đơn


### 3. TTPP thường có thể view đơn đơn trả giữ hộ của TTPP logistic
#### Nhu cầu
```
TTPP thường được xem nhưng không được thao tác trên đơn trả giữ hộ
```

#### Thiết kế
Nhu cầu này sẽ giải quyết bằng 2 tính năng:

- Phân quyền data
- Phân quyền tính năng

Với mỗi account thuộc loại TTPP tương ứng:

- Đối với account TTPP thường:
  + Gán vào nhóm phân quyền TTPP thường (view nhưng không thao tác được trên đơn trả giữ hộ)
  + phân quyền data thấy được TTPP thường + TTPP logistic tương ứng
- Đối với account TTPP logistic
  + Gán vào nhóm phân quyền TTPP logistic (full quyền trên các loại đơn)
  + phân quyền data thấy được TTPP logistic

#### Impact
- Cần Tạo mới account và phân bổ xuống cho nhân viên TTPP

### 4. Quản lí mapping giữa CN-TTPP
#### Nhu cầu
```
Tool admin quản lí được mapping giữa CN-TTPP
```

#### Thiết kế
Trong chi tiết Chi Nhánh

Bổ sung bảng các TTPP được mapping gồm các cột:

- Nghành
- Tên TTPP
- Mã TTPP

Yêu cầu:

- List ra đủ các nghành
- Nghành nào chưa có TTPP sẽ list TTPP rỗng

Có thể edit và thêm vào mapping mới.

Trong chi tiết TTPP

Bổ sung bảng các CN được mapping gồm các cột:
- Nghành
- Tên Chi nhánh
- Mã chi nhánh

Yêu cầu:

- List ra đủ các nghành
- Nghành nào chưa có CN sẽ list CN rỗng

Có thể edit và thêm vào mapping mới.

**VD:**

Chi tiết CN1

| Ngành | Tên TTPP | Mã TTPP |
|-------|----------|---------|
| Giống | TTPP A   | A       |
| Giống | TTPP B   | B       |
| Phân  | TTPP C   | C       |
| Thuốc | _        | _       |

Chi tiết CN2

| Ngành | Tên TTPP | Mã TTPP |
|-------|----------|---------|
| Giống | TTPP B   | B       |
| Phân  | TTPP C   | C       |
| Thuốc | TTPP B   | B       |


Chi tiết TTPP A

| Ngành | Tên CN | Mã CN |
|-------|----------|---------|
| Giống | CN 1   | 1       |
| Phân  | _   | _       |
| Thuốc | _        | _       |

Chi tiết TTPP B

| Ngành | Tên CN | Mã CN |
|-------|----------|---------|
| Giống | CN1  | 1 |
| Giống | CN2  | 2 |
| Phân  | _   | _       |
| Thuốc | CN2        | 2       |

### 5. Danh sách MQH TTPP - TTPP logistic 
Làm danh sách có các cột sau

| Mã TTPP | Tên TTPP | Loại đơn | Nghành | Mã TTPP Logistic | Tên TTPP Logistic
|---|---|---|---|---|---|
| A | TTPP A | Trả giữ hộ | Giống | L_A | TTPP L_A |
| A | TTPP A | Trả giữ hộ | Phân | L_A | TTPP L_A |
| B | TTPP B | _ | _ | _ | _ |
| _ | _ | _ | _ | L_B | TTPP L_B |

2 Thanh search:

- Search tên TTPP thường
- Search tên TTPP logistic

Có thể edit mapping

Filter:

- Trạng thái gán:
  - Đã gán
  - TTPP thường chưa gán TTPP logistic
  - TTPP logistic chưa gán TTPP thường

### 6. Đồng bộ đại lý C1
=> SAP chưa gửi tài liệu. Sẽ thêm key gì ?

SAP sẽ sync thêm chi nhánh về, nRMS dùng chi nhánh này làm chi nhánh cho đại lý.

Nếu SAP đẩy về đại lý có nhiều CN quản lý thì lấy dữ liệu chi nhánh cuối cùng.

### 7. Chuyển luồng giữ hộ qua SAP

#### Sync các loại đơn sang TMS
Trong API sync đơn sang TMS sẽ gửi thêm mã TTPP vào key DO_Code

#### Sync đơn trả giữ hộ sang SAP & TMS

Chi tiết trong tài liệu FS của SAP.
PO đính kèm tài liệu vào build.
Trong đó có 2 key:
- Purchasing Org
- Company
SAP gửi data để lưu vào bảng `warehouse_center_tower`

**Update DB**

```
Bảng warehouse_center_tower bổ sung 2 cột purchasing_org và company
```

Trước khi gửi đơn qua SAP sẽ dùng thông tin kho điều phối và TTPP điều phối để tra lấy 2 trường
thồng tin này

#### Sync tồn kho giữ hộ từ SAP

nRMS viết API
SAP sẽ sync tồn kho về khi có thay đổi.

**Cách thức sync**

Khi có thay đổi về tồn kho giữ hộ, SAP sẽ gửi thông tin về cho nRMS.
Tồn kho này là số lượng final trong kho giữ hộ của đại lý.

| Lô | Đại lý | Sản phẩm | Số lượng | Tháng&Năm
|-----|------|-----| ----| --- |
| L1 | A   | SP1 | 20 | 2021-08-10
| L2 | A   | SP1 | 10 | 2021-08-10
| L3 | A   | SP1 | 10 | 2021-08-11

nRMS dùng trường Tháng&Năm làm mã lô, và sum theo Tháng&Năm

Sau đó dùng kết quả đã sum và cập nhật vào tồn kho đại lý

Tồn kho sau khi sum.

| Lô | Đại Lý | Sản phẩm | Số lượng
| --- | --- | --- | --- |
| 2021-08-10 | A | SP1 | 30
| 2021-08-11 | A | SP1 | 10

Data SAP sync về lưu log để khi cần sẽ tra cứu.

#### Điều chỉnh luồng ngậm đơn khi thay đổi cách sync

Tồn kho giữ hộ bị ngậm khi đơn hàng ở trạng thái từ `đơn mới` -> `chuyển giao hàng`

Khi đơn sang trạng thái `hoàn thành` => ngưng ngậm đơn

Vì lúc này đơn đã được SAP bắn SO chuyển trạng thái hoàn thành và SAP sẽ tự động sync tồn kho mới
của đại lý về.

#### SO status chuyển hoàn thành cho đơn trả giữ hộ

Đơn trả giữ hộ sẽ làm thêm chuyển trạng thái hoàn thành dựa vào SO Status.

### 8. Sync công nợ theo nghành

Chưa có thông tin tài liệu từ SAP

### 9. Tách nghành cho các loại đơn trả

Các loại đơn sẽ tách nghành:

- Đơn trả giữ hộ
- Đơn trả ký gửi

Impact

- Check lại luồng ngậm đơn ở giữ hộ và ký gửi khi tách nghành
